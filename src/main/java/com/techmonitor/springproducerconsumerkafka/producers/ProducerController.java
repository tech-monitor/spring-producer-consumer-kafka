package com.techmonitor.springproducerconsumerkafka.producers;

import com.techmonitor.springproducerconsumerkafka.models.Employee;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/produce")
public class ProducerController {

    private final KafkaTemplate<String,Object> kafkaTemplate;

    @Value("${topic-name}")
    private String topicName;

    public ProducerController(KafkaTemplate<String, Object> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @PostMapping("/employee")
    public ResponseEntity<String> produceMessage(@RequestBody Employee employee){

        try {
            kafkaTemplate.send(topicName, employee);
            return ResponseEntity
                    .status(HttpStatus.CREATED).body("Message was sent with success");
        }catch (Exception exception){
            exception.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.SERVICE_UNAVAILABLE)
                    .body("Message was not sent");
        }

    }
}
