package com.techmonitor.springproducerconsumerkafka.consumers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techmonitor.springproducerconsumerkafka.models.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class EmployeeConsumer {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeConsumer.class);

    @Value("${consumer-group-id}")
    private String groupId;

    @Value("${topic-name}")
    private String topicName;

    @KafkaListener(groupId = "${consumer-group-id}", topics = "${topic-name}", containerFactory = "kafkaListenerContainerFactory")
    public void consume(Employee employee) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonEmployee = mapper.writeValueAsString(employee);
        logger.info("Message consumed: " + jsonEmployee);
    }

}
