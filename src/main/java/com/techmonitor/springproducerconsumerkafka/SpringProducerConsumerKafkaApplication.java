package com.techmonitor.springproducerconsumerkafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringProducerConsumerKafkaApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringProducerConsumerKafkaApplication.class, args);
    }
}
